import React, { Component } from "react";
import { Link } from "react-router-dom";
import Loader from "../../partials/loader/Loader";
import Input from "../../partials/Input/Input";
import Button from "../../partials/Button/Button";
import Axios from "axios";
import { getCryptoList } from "../../services/Services";
import "./CryptoTable.css";

class cryptoTable extends Component {
  state = {
    data: [],
    currentCryptos: [],
    inputId: "",
    inputVal: "",
    amount: 0,
    yourPrice: [],
    localDataPrice: [],
    initPrice: 0,
    loader: true,
    focusedId: 0,
    currentPage: 1,
    cryptosPerPage: 5
  };
  componentDidMount() {
    // await setInterval(() => {
    //   getCryptoList();
    // }, 6000);
    getCryptoList()
      .then(response => {
        let getLocalData = JSON.parse(localStorage.getItem("coinValue"));
        let initPriceValue = response.data.map(el => ({
          id: el.id,
          price: 0
        }));

        this.setState(
          {
            data: response.data,
            loader: false,
            yourPrice: getLocalData ? getLocalData : initPriceValue
          },
          () => this.getCryptoPagination()
        );
      })
      .catch(err => {
        console.log(err);
      });
  }
  getYourPriceHandler = (e, price, id) => {
    e.preventDefault();
    let yourPrice = [];
    if (this.state.yourPrice) {
      yourPrice = [...this.state.yourPrice];

      for (var i = 0; i < yourPrice.length; i++) {
        console.log(yourPrice[i]);
        let objToDel = yourPrice[i];
        if (id.indexOf(objToDel.id) !== -1) {
          yourPrice.splice(i, 1);
        }
      }
    }
    let calcYourPrice = {
      id: id,
      price:
        Number.parseFloat(price).toFixed(2) *
        Number.parseFloat(this.state.amount).toFixed(2)
    };

    yourPrice.push(calcYourPrice);
    this.setState({
      yourPrice: yourPrice
    });
    localStorage.setItem("coinValue", JSON.stringify(yourPrice));
  };

  inputChangeHandler = (e, id) => {
    if (e.target.value > 0) {
      let inputs = [];

      inputs.push({
        val: e.target.value,
        id: id
      });

      this.setState({
        inputId: inputs[0].id,
        amount: inputs[0].val
      });
    }
  };

  getCryptoPagination = () => {
    const indexOfLastCrypto =
      this.state.currentPage * this.state.cryptosPerPage;
    const indexOfFirstCrypto = indexOfLastCrypto - this.state.cryptosPerPage;
    const currentCryptos = this.state.data.slice(
      indexOfFirstCrypto,
      indexOfLastCrypto
    );
    console.log(currentCryptos);
    this.setState({
      currentCryptos: currentCryptos
    });
  };
  handleClick = e => {
    e.preventDefault();
    this.setState(
      {
        currentPage: +e.target.dataset.value
      },
      () => this.getCryptoPagination()
    );
  };
  render() {
    let table = null;
    if (!this.state.loader) {
      table = this.state.currentCryptos.map(crypto => {
        return (
          <tr key={crypto.name}>
            <td>{crypto.market_cap_rank}</td>
            <td>
              <img src={crypto.image} alt={crypto.name} title={crypto.name} />
            </td>
            <td>
              <Link to={`/detailInfo/${crypto.id}`}>{crypto.name}</Link>
            </td>
            <td>{crypto.symbol}</td>
            <td>$ {Number.parseFloat(crypto.current_price).toFixed(2)}</td>
            <td>
              <span
                className={
                  crypto.price_change_percentage_24h < 0 ? "lower" : "higher"
                }
              >
                {Number.parseFloat(crypto.price_change_percentage_24h).toFixed(
                  2
                )}
                %
              </span>
            </td>
            <td>
              <form
                onSubmit={e =>
                  this.getYourPriceHandler(e, crypto.current_price, crypto.id)
                }
              >
                <input
                  type="number"
                  id={crypto.id}
                  value={
                    this.state.inputId === crypto.id &&
                    this.state.inputVal !== ""
                      ? this.state.inputVal
                      : ""
                  }
                  onFocus={() => this.setState({ inputVal: "" })}
                  min="0"
                  onClick={() => this.setState({ inputId: crypto.id })}
                  onChange={e =>
                    this.setState({
                      amount: e.target.value,
                      inputVal: e.target.value
                    })
                  }
                />

                <button
                  className="btn"
                  type="submit"
                  disabled={
                    this.state.inputId !== crypto.id && this.state.amount !== 0
                      ? true
                      : false
                  }
                >
                  Submit
                </button>
              </form>
            </td>
            <td className="coinPrice" id={"coin-" + crypto.id}>
              {this.state.yourPrice
                ? this.state.yourPrice.map((data, i) => {
                    if (crypto.id === data.id) {
                      return data.price;
                    } else {
                      return null;
                    }
                  })
                : null}
              $
            </td>
          </tr>
        );
      });
    }
    // pagination
    const pageNumbers = [];
    for (
      let i = 1;
      i <= Math.ceil(this.state.data.length / this.state.cryptosPerPage);
      i++
    ) {
      pageNumbers.push(i);
    }
    const renderPageNumbers = pageNumbers.map((number, id) => {
      return (
        <li
          key={id + 1}
          id={id + 1}
          className={
            id + 1 === this.state.currentPage ? "active" : "waves-effect"
          }
        >
          <a href="#" data-value={number} onClick={this.handleClick}>
            {number}
          </a>
        </li>
      );
    });

    // END pagination
    return (
      <div className="row">
        <div className="col s12">
          {this.state.loader ? <Loader /> : null}
          <table className="striped responsive-table">
            <thead>
              <tr>
                <th />
                <th />
                <th>Name</th>
                <th>Short Name</th>
                <th>$ Value</th>
                <th>last 24h</th>
                <th>Amount you own</th>
                <th>$ value of your coin</th>
              </tr>
            </thead>
            <tbody>{table}</tbody>
          </table>
          <ul className="pagination">
            {renderPageNumbers}

            {/* <li className="disabled">
              <a href="#!">
                <i className="material-icons">chevron_left</i>
              </a>
            </li>
            <li className="active">
              <a href="#!">1</a>
            </li>
            <li className="waves-effect">
              <a href="#!">2</a>
            </li>
            <li className="waves-effect">
              <a href="#!">3</a>
            </li>
            <li className="waves-effect">
              <a href="#!">4</a>
            </li>
            <li className="waves-effect">
              <a href="#!">5</a>
            </li>
            <li className="waves-effect">
              <a href="#!">
                <i className="material-icons">chevron_right</i>
              </a>
            </li> */}
          </ul>
        </div>
      </div>
    );
  }
}

export default cryptoTable;
