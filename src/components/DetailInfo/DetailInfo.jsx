import React, { Component } from "react";
import Loader from "../../partials/loader/Loader";
import "./DetailInfo.scss";

import { getCryptoDetail } from "../../services/Services";

class DetailInfo extends Component {
  state = {
    dataDetail: {},
    loader: true
  };
  componentDidMount() {
    // this.getAxios();
    getCryptoDetail(this.props.match.params.id)
      .then(response => {
        console.log(response);
        this.setState({
          dataDetail: response,
          loader: false
        });
      })
      .catch(err => {
        console.log(err);
      });
  }
  render() {
    console.log(this.props);
    const crypto = this.state.dataDetail;
    console.log(this.props);
    return (
      <div className="row detail-info">
        {this.state.loader ? <Loader /> : null}

        <div className="col s12 valign-wrapper">
          <img src={crypto.image} alt={crypto.name} title={crypto.name} />
          <h2>
            {crypto.name} <span>({crypto.symbol})</span>
          </h2>

          <hr />
        </div>
        <div className="col s12">
          <p>
            <strong>Market Cap rank : </strong>
          </p>
          <p>
            <span className="rank">Rank #{crypto.market_cap_rank}</span>
          </p>
        </div>
        <div className="col s12">
          <p>
            <strong>Price : </strong>
          </p>
          <p>
            1 {crypto.symbol} =
            <span className="price">
              {Number.parseFloat(crypto.current_price).toFixed(2)} $
              <span
                className={
                  crypto.price_change_percentage_24h < 0 ? "lower" : "higher"
                }
              >
                ({crypto.price_change_24h})
              </span>
            </span>
          </p>
        </div>
        <div className="col s12">
          <p>
            <strong>Changes % in last 24h : </strong> =
            <span
              className={
                crypto.price_change_percentage_24h < 0 ? "lower" : "higher"
              }
            >
              {Number.parseFloat(crypto.price_change_percentage_24h).toFixed(2)}
              %
            </span>
          </p>
        </div>
        <div className="col s12">
          <p>
            <strong>Changes price in last 24h : </strong> =
            <span
              className={
                crypto.price_change_percentage_24h < 0 ? "lower" : "higher"
              }
            >
              {Number.parseFloat(crypto.price_change_24h).toFixed(2)}$
            </span>
          </p>
        </div>
      </div>
    );
  }
}

export default DetailInfo;
