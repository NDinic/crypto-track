import Axios from "axios";
const BASE_URL =
  "https://api.coingecko.com/api/v3/coins/markets?vs_currency=usd";
//const BASE_URL = 'https://api.coinmarketcap.com/v1/ticker/?limit=50'
export const getCryptoList = () => {
  return Axios.get(BASE_URL)
    .then(response => response)
    .catch(err => err);
};

export const getCryptoDetail = id => {
  return Axios.get(BASE_URL)
    .then(response => {
      let getCryptoDetail = "";
      response.data.map(crypto => {
        if (crypto.id === id) {
          getCryptoDetail = crypto;
        }
        return getCryptoDetail;
      });
      return getCryptoDetail;
    })
    .catch(err => err);
};
