import React from "react";
import "./Footer.css";
const footer = () => {
  return (
    <div className="footer teal lighten-2">
      <div className="container">
        <div className="row">
          <div className="col 12">
            <h4>Crypto world - footer</h4>
          </div>
        </div>
      </div>
    </div>
  );
};

export default footer;
