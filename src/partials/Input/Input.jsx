import React from "react";

const inputFiled = props => {
  return (
    <input
      type="number"
      id={props.id}
      onChange={e => props.changed(e, props.id)}
    />
  );
};

export default inputFiled;
