import React from "react";

const buttonFiled = props => {
  return (
    <button
      type={props.type}
      id={props.id}
      className="btn"
      disabled={!props.isDisabled}
    >
      Submit
    </button>
  );
};

export default buttonFiled;
