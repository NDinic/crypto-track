import React from "react";
import { Link } from "react-router-dom";
import "./Header.css";

const header = () => {
  return (
    <div className="header teal lighten-2">
      <div className="container">
        <div className="row">
          <div className="col 12">
            <h2>
              <Link to="/"> Crypto world</Link>
            </h2>
          </div>
        </div>
      </div>
    </div>
  );
};

export default header;
