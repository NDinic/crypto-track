import React, { Component, Fragment } from "react";
import "./App.scss";

import Header from "./partials/header/Header";
import Table from "./components/CryptoTable/CryptoTable";
import Footer from "./partials/footer/Footer";
import DetailInfo from "./components/DetailInfo/DetailInfo";
import { Route } from "react-router-dom";
class App extends Component {
  render() {
    return (
      <Fragment>
        <Header />
        <div className="container">
          <Route path="/detailInfo/:id" component={DetailInfo} />
          <Route path="/" exact component={Table} />
        </div>
        <Footer />
      </Fragment>
    );
  }
}

export default App;
